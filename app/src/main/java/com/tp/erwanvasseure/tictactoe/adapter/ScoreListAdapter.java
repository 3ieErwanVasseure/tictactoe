package com.tp.erwanvasseure.tictactoe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tp.erwanvasseure.tictactoe.R;
import com.tp.erwanvasseure.tictactoe.db.Score;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by erwan.vasseure on 04/02/2015.
 */
public class ScoreListAdapter extends BaseAdapter
{

    private List<Score> data;
    private Context context;

    public ScoreListAdapter(List<Score> data, Context context)
    {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public String getItem(int position)
    {
        return data.get(position).winner;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    protected class ScoreViewHolder
    {
        @InjectView(R.id.score_layout_winner)
        protected TextView winnerTextview;

        ScoreViewHolder(final View rootView)
        {
            ButterKnife.inject(this, rootView);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View rowView;
        ScoreViewHolder viewHolder;
        if (convertView == null)
        {
            rowView = LayoutInflater.from(context).inflate(R.layout.score_layout, parent, false);
            viewHolder = new ScoreViewHolder(rowView);
            rowView.setTag(viewHolder);
        } else
        {
            rowView = convertView;
            viewHolder = (ScoreViewHolder) rowView.getTag();
        }
        viewHolder.winnerTextview.setText(getItem(position));

        return rowView;
    }
}
