package com.tp.erwanvasseure.tictactoe.db;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by erwan.vasseure on 04/02/2015.
 */

@Table(name = "Scores")
public class Score extends Model
{

    @Column(name = "Winner")
    public String winner;

    public Score(String winner)
    {
        super();
        this.winner = winner;
    }

    public Score()
    {
        super();
    }
}