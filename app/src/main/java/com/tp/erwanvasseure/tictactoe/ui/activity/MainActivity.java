package com.tp.erwanvasseure.tictactoe.ui.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.tp.erwanvasseure.tictactoe.R;
import com.tp.erwanvasseure.tictactoe.ui.fragment.GameFragment;
import com.tp.erwanvasseure.tictactoe.listener.ActionBarListener;


public class MainActivity extends ActionBarActivity implements GameFragment.onInteractionGameFrag
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBarListener actionBarListener = new ActionBarListener();
        ActionBar aBar = getSupportActionBar();
        aBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        aBar.setCustomView(R.layout.action_bar_layout);
        aBar.addTab(aBar.newTab().setTabListener(actionBarListener)
                .setText("Jeu"));

        aBar.addTab(aBar.newTab().setTabListener(actionBarListener)
                .setText("Score"));

        aBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void onClickRestart()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, new GameFragment())
                .commit();
    }
}
