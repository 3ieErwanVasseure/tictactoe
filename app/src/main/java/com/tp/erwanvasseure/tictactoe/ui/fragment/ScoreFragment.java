package com.tp.erwanvasseure.tictactoe.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.tp.erwanvasseure.tictactoe.R;
import com.tp.erwanvasseure.tictactoe.adapter.ScoreListAdapter;
import com.tp.erwanvasseure.tictactoe.db.Score;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ScoreFragment extends Fragment {

    @InjectView(R.id.fragment_score_listView)
    protected ListView listView;

    @InjectView(R.id.fragment_score_text_lvempty)
    protected TextView textLVEmpty;

    List<Score> data;

    public ScoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score, container, false);
        ButterKnife.inject(this, view);

        listView.setEmptyView(textLVEmpty);

        List<Score> data = new Select()
                .from(Score.class)
                .execute();

        ScoreListAdapter adapter = new ScoreListAdapter(data, getActivity());
        listView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
}
