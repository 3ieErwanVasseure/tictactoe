package com.tp.erwanvasseure.tictactoe.listener;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.tp.erwanvasseure.tictactoe.R;
import com.tp.erwanvasseure.tictactoe.ui.fragment.GameFragment;
import com.tp.erwanvasseure.tictactoe.ui.fragment.ScoreFragment;

/**
 * Created by erwan.vasseure on 04/02/2015.
 */
public class ActionBarListener implements ActionBar.TabListener
{
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        if (tab.getText().equals("Jeu"))
            fragmentTransaction
                    .replace(R.id.main_container, new GameFragment());
        else
            fragmentTransaction
                    .replace(R.id.main_container, new ScoreFragment());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {

    }
}