package com.tp.erwanvasseure.tictactoe.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.tp.erwanvasseure.tictactoe.R;
import com.tp.erwanvasseure.tictactoe.db.Score;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by erwan.vasseure on 04/02/2015.
 */
public class GameFragment extends Fragment
{

    @InjectView(R.id.turn)
    protected TextView textViewTurn;

    @InjectView(R.id.fragment_game_score)
    protected TextView textViewScore;

    @InjectView(R.id.button_restart)
    protected TextView buttonRestart;

    private onInteractionGameFrag mListener;
    private boolean circleTurn = true;
    private boolean gameOver = false;
    private int[] cases;

    public GameFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_game, container, false);
        cases = new int[9];
        for (int i = 0; i < 9; ++i)
            cases[i] = -1;
        ButterKnife.inject(this, view);

        textViewScore.setText((new Select()
                .from(Score.class)
                .where("winner = ?", "X")
                .execute()).size() + " - " +
                (new Select()
                        .from(Score.class)
                        .where("winner = ?", "O")
                        .execute()).size());


        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (onInteractionGameFrag) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    public interface onInteractionGameFrag
    {
        public void onClickRestart();
    }

    @OnClick(R.id.button_restart)
    public void restart()
    {
        if (mListener != null)
            mListener.onClickRestart();
    }

    @OnClick({R.id.case_1_1, R.id.case_2_1, R.id.case_3_1,
            R.id.case_1_2, R.id.case_2_2, R.id.case_3_2,
            R.id.case_1_3, R.id.case_2_3, R.id.case_3_3})
    public void placeMark(ImageView pos)
    {
        if (pos.getDrawable() == null && !gameOver)
        {
            int i = Integer.parseInt(pos.getTag().toString());
            if (circleTurn)
            {
                pos.setImageResource(R.drawable.ic_circle);
                textViewTurn.setText("A la croix de jouer !");
                cases[i - 1] = 0;
            } else
            {
                pos.setImageResource(R.drawable.ic_cross);
                textViewTurn.setText("Au rond de jouer !");
                cases[i - 1] = 1;
            }
            circleTurn = !circleTurn;
            int result = checkGameState();
            if (result != -1)
            {
                Score score = new Score("-");

                if (result == 0)
                {
                    textViewTurn.setText("Le rond a gagné !");
                    score.winner = "O";
                } else if (result == 1)
                {
                    textViewTurn.setText("La croix a gagné !");
                    score.winner = "X";
                } else
                    textViewTurn.setText("Oh les mauvais !");

                score.save();
                gameOver = true;
                buttonRestart.setVisibility(View.VISIBLE);
            }
        }
    }

    private int checkGameState()
    {
        // A OPTIMISER
        if (cases[0] == cases[1] && cases[1] == cases[2])
            return cases[0];
        if (cases[3] == cases[4] && cases[4] == cases[5])
            return cases[3];
        if (cases[6] == cases[7] && cases[7] == cases[8])
            return cases[6];
        if (cases[0] == cases[3] && cases[3] == cases[6])
            return cases[0];
        if (cases[1] == cases[4] && cases[4] == cases[7])
            return cases[1];
        if (cases[2] == cases[5] && cases[5] == cases[8])
            return cases[2];
        if (cases[0] == cases[4] && cases[4] == cases[8])
            return cases[0];
        if (cases[6] == cases[4] && cases[4] == cases[2])
            return cases[6];
        int j;
        for (j = 0; j < 9 && cases[j] != -1; ++j) ;
        if (j == 9)
            return 2;
        return -1;
    }
}